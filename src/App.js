import { Header } from './components/Header';
import { BrowserRouter } from "react-router-dom";
import {useEffect, useState} from 'react';
import { Main } from './pages/Main';
import "./styles/app.css"
import axios from "axios";
function App() {
  const [auth,setAuth] = useState(false);
  const [currentPage,setCurrentPage] = useState("Start");
  const [user, setUser] = useState(undefined);
  const [totalRoutes, setTotalRoutes] = useState(undefined)

    useEffect(() => {
        if (!auth)
            setTotalRoutes(['Noch keine Fahrten vorhanden',])
    }, [auth]);
    useEffect(() => {
        const loggedInUser = localStorage.getItem("user");
        if (loggedInUser) {
            const foundUser = JSON.parse(loggedInUser);
            axios.post('/login', {id: foundUser.id, password: foundUser.password})
                .then((res) => {
                    res = res.data.user
                    const user = { id: res.id, email: res.email, password: res.password, co2Emission: res.co2Emission };
                    // set the state of the user
                    // store the user in localStorage
                    localStorage.setItem('user', JSON.stringify(user))
                    setUser(user)
                    setAuth(true)
                })
                .catch(error => alert('Login failed'))
        }
    }, []);
  return (
    <BrowserRouter>
     <div className="App">
          <Header setUser={setUser} auth={auth} setAuth={setAuth} currentPage={currentPage} setCurrentPage={setCurrentPage}></Header>
          <Main totalRoutes={totalRoutes} setTotalRoutes={setTotalRoutes} user={user} setAuth={setAuth} setCurrentPage={setCurrentPage} currentPage={currentPage} auth={auth} setUser={setUser}></Main>
    </div>
    </BrowserRouter>
   
  );
}

export default App;
