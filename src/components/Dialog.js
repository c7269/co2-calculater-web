import * as React from "react";
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogTitle from "@mui/material/DialogTitle";
import useMediaQuery from "@mui/material/useMediaQuery";
import { useTheme } from "@mui/material/styles";
import Map from "./MapComponent";
import {makeStyles} from "@mui/styles";

const useStyles = makeStyles({
  dialogContent: {
    width: 500,
    height: 500,
    ['@media only screen and (max-device-width: 480px)']: {
      width: "100vw",
      height: "55vh",
    },
  },
  dialog: {
    ['@media only screen and (max-device-width: 480px)']: {
      marginLeft: "2.5vw",
      width: "95vw",
      height: "55vh",
      marginTop: "10vh"
    },

  }

})

export default function DialogContainer(props) {
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);
  const theme = useTheme();
  const fullScreen = useMediaQuery(theme.breakpoints.down("md"));

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    const start = document.getElementsByClassName("mapboxgl-ctrl-geocoder")[0]
      .children[1].value;

    const destination = document.getElementsByClassName(
      "mapboxgl-ctrl-geocoder"
    )[1].children[1].value;

    if (start === "" || destination === "") {
      setOpen(false);
      return;
    }
    props.setStartPoint({ label: start });
    props.setEndPoint({ label: destination });
    const x = document.getElementsByClassName(
      "mapbox-directions-route-summary"
    );
    let km = 0;
    let duration = 0;
    if (x.length > 0) {
      let d = 0;
      if (x[0].children[2] && x[0].children[2].innerHTML) {
        d = x[0].children[2].innerHTML;
        km = Number(x[0].children[1].innerHTML.split("mi")[0]);
      } else {
        d = x[0].children[1].innerHTML;
        km = Number(x[0].children[0].innerHTML.split("mi")[0]);
      }
      const min = Number(d.match(/[0-9]*min/gm)[0].split("min")[0]);
      const h = Number(d.match(/[0-9]*h/gm)[0].split("h")[0]);
      duration = h * 60 + min;
      props.setKilometers(Math.round((km + Number.EPSILON) * 10) / 10);
    }

    var distance = km;
    var durationCar = Number(duration);

    var rowCopy = props.rows;
    rowCopy[0].Pollution =
      Math.round(
        ((rowCopy[0].Factor * distance) / 1000 + Number.EPSILON) * 100
      ) / 100;
    rowCopy[0].Time = Math.round(durationCar + Number.EPSILON);
    rowCopy[1].Pollution =
      Math.round(
        ((rowCopy[1].Factor * distance) / 1000 + Number.EPSILON) * 100
      ) / 100;
    rowCopy[2].Pollution =
      Math.round(
        ((rowCopy[2].Factor * distance) / 1000 + Number.EPSILON) * 100
      ) / 100;
    rowCopy[3].Time = Math.round((distance / 17) * 60 + Number.EPSILON);

    props.setRows(rowCopy);
    props.isLoading(false);
    props.setClicked(false);
    setOpen(false);
  };

  return (
    <div>
      <Button
        variant="outlined"
        onClick={handleClickOpen}
        style={{width: "100%", color: "black", borderColor: "black", borderRadius: "1em"}}
      >
        Karte öffnen
      </Button>
      <Dialog
        fullScreen={fullScreen}
        open={open}
        onClose={handleClose}
        aria-labelledby="responsive-dialog-title"
        className={classes.dialog}
      >
        <DialogContent>
          <div className={classes.dialogContent}>
            <Map></Map>
          </div>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} autoFocus>
            Schließen
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
