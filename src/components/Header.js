import { Button, Tab, Tabs } from "@mui/material";
import React, { useEffect } from "react";
import "../styles/header.css";

export const Header = (props) => {
  const handleChange = (event, newValue) => {
    props.setCurrentPage(newValue);
  };

  const renderAuthenticationButtons = () => {
    if (!props.auth) {
      return (
        <>
          <Button
            id="loginButton"
            onClick={() => {
              handleChange(null, "Login");
            }}
          >
            Einloggen
          </Button>

          <Button
            id="registerButton"
            onClick={() => {
              handleChange(null, "Signup");
            }}
          >
            Registrieren
          </Button>
        </>
      );
    }
    return (
      <>
        <Button
          variant="text"
          id="logoutButton"
          onClick={() => {
            props.setCurrentPage("Setup");
          }}
        >
          Settings
        </Button>
        <Button
          variant="text"
          id="logoutButton"
          onClick={() => {
            localStorage.removeItem("user");
            props.setAuth(false);
            props.setCurrentPage("Start");
          }}
        >
          Ausloggen
        </Button>
      </>
    );
  };
  const renderTabsContent = () => {
    return (
      <>
        <Tabs
          variant="standard"
          style={{
            height: "100%",
            justifyContent: "center",
            alignItems: "center",
            width: "100%",
            flex: 1,
          }}
          value={props.currentPage}
          onChange={handleChange}
          TabIndicatorProps={{
            style: {
              backgroundColor: "transparent",
            },
          }}
          TabScrollButtonProps={{ style: { backgroundColor: "#fff" } }}
        >
          <Tab
            value="Start"
            id="tabs"
            label="Start"
            style={{
              backgroundColor:
                props.currentPage === "Start" ? "#3e3b3b" : "#302f2f",
            }}
          />
          {props.auth ? (
            <Tab
              value="Ranking"
              id="tabs"
              label="Ranking"
              style={{
                backgroundColor:
                  props.currentPage === "Ranking" ? "#3e3b3b" : "#302f2f",
              }}
            />
          ) : (
            ""
          )}
          <Tab
            value="Route planen"
            id="tabs"
            label="Route planen"
            style={{
              backgroundColor:
                props.currentPage === "Route planen" ? "#3e3b3b" : "#302f2f",
            }}
          />
          {props.auth ? (
            <Tab
              value="Route hinzufügen"
              id="tabs"
              label="Route hinzufügen"
              style={{
                backgroundColor:
                  props.currentPage === "Route hinzufügen"
                    ? "#3e3b3b"
                    : "#302f2f",
              }}
            />
          ) : (
            ""
          )}
        </Tabs>
      </>
    );
  };

  return (
    <div className="headerBar">
      <div className="leftDiv"></div>
      <div className="bar">{renderTabsContent()}</div>
      <div className="rightDiv">{renderAuthenticationButtons()}</div>
    </div>
  );
};
