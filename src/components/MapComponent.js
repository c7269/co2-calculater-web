import React, { useEffect, useRef, useState } from "react";
import mapboxgl from "mapbox-gl"; // eslint-disable-line import/no-webpack-loader-syntax
import MapboxDirections from "@mapbox/mapbox-gl-directions/dist/mapbox-gl-directions";
import "mapbox-gl/dist/mapbox-gl.css";
import "@mapbox/mapbox-gl-directions/dist/mapbox-gl-directions.css";

mapboxgl.accessToken =
  "pk.eyJ1IjoibWFqZC1hZGF3aWVoIiwiYSI6ImNrN3lpNGoyNDAxemIzZHFrYTY4cDNjd3QifQ.EA5Ea8XrWVSAu7bejjtjRQ";

const Map = (props) => {
  const mapContainer = useRef(null);
  const map = useRef(null);
  const [lng, setLng] = useState(-70.9);
  const [lat, setLat] = useState(42.35);
  const [zoom, setZoom] = useState(9);

  useEffect(() => {
    if (map.current) return; // initialize map only once
    map.current = new mapboxgl.Map({
      container: mapContainer.current,
      style: "mapbox://styles/mapbox/streets-v11",
      center: [lng, lat],
      zoom: zoom,
      steps: false,
      banner_instructions: false,
      width: "100%",
      height: "100%"
    });

    const directions = new MapboxDirections({
      accessToken: mapboxgl.accessToken,
      profile: "mapbox/driving",
      alternatives: true,
      steps: false,
      banner_instructions: false,
    });

    map.current.addControl(directions, "top-left");
  });

  return (
    <div ref={mapContainer} style={{ height: "100%", width: "100%" }}></div>
  );
};

export default Map;
