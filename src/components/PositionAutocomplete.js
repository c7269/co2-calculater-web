import * as React from 'react';
import TextField from '@mui/material/TextField';
import Autocomplete from '@mui/material/Autocomplete';
import {useState} from "react";
import axios from "axios";
import {makeStyles} from "@mui/styles";

const useStyles = makeStyles({
    textField: {
        fontFamily: "'Lato', sans-serif",
        height: "55px",
        fontSize: "1em",
        ['@media only screen and (max-device-width: 480px)']: {
            height: "45px",
            fontSize: "0.5em"
        },
        display: "flex",
        justifyContent: "center"
    },
    dropdown: {
        fontFamily: "'Lato', sans-serif",
        fontSize: "1em",
        ['@media only screen and (max-device-width: 480px)']: {
            fontSize: "0.5em",
        },
    },
    additionalStyles: {
        height: "55px",
        ['@media only screen and (max-device-width: 480px)']: {
            height: "45px",
        },
    }
});

export default function PositionAutocomplete(props) {
    const classes = useStyles();
    const [state, setState] = useState("");
    const [options, setOptions] = useState([])
    const handleChange = (value) => {
        if(value === undefined)
            setOptions([])
        props.setSelectedOption(value);
    }
    const updateAutocomplete = async (value) => {
        try {
            value = value.replaceAll(" ", "+")
            var response = await axios.get(`https://autocomplete.geocoder.ls.hereapi.com/6.2/suggest.json?apiKey=EEAnGX6cMH5_ScGwLQqU40IYYGacZDojipUmbpVCHdg&query=`+value)
            response = response.data.suggestions
            var tmp = []
            const len = response.length
            for (var i=0; i < len; i++) {
                tmp.push(
                    {
                        label: response[i].label,
                        locationId: response[i].locationId
                    })
            }
            setOptions(tmp)
            setState(value)
        } catch (error) {
            console.log('error:', error);
        }
    }
    return (
        <>
            <Autocomplete
                className="autoComplete"
                classes={{
                    option: classes.dropdown,
                    root: classes.additionalStyles
                }}
                options={options}
                onChange={(event, value) => {
                    handleChange(value) }}
                renderInput={
                    params =>
                        <TextField
                            onChange={(e) => updateAutocomplete(e.target.value)}
                            value={state}
                            variant="standard"
                            {...params}
                            InputProps={{...params.InputProps, disableUnderline: true, classes: {root: classes.textField}}}
                        />}
                />
        </>

    );
}

