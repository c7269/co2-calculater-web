import { LandingPage } from "./subpages/LandingPage";
import { Login } from "./subpages/Login";
import { Overview } from "./subpages/Overview";
import { Ranking } from "./subpages/Ranking";
import { Signup } from "./subpages/Signup";
import { RoutePlanner } from "./subpages/RoutePlanner";
import { AddRoute } from "./subpages/AddRoute";
import background from "../images/background.png";
import {BrowserView} from 'react-device-detect';

import "../styles/main.css";
import { Impressum } from "./subpages/Impressum";
import { Button, Link } from "@mui/material";
import React from "react";
import { Setting } from "./subpages/Setting";
import Picture1 from "../images/cooperations/MittelstandDigital_Single_CMYK.png";
import Picture2 from "../images/cooperations/logo_md40_kompetenzzentren_magdeburg_RGB.png";
import Picture3 from "../images/cooperations/Logo-Tourismusnetzwerk.png";
import Picture4 from "../images/cooperations/Logo-SUNK.png";
import Picture5 from "../images/cooperations/Logo-Lena.png";
import Picture6 from "../images/cooperations/Logo-LTV.png";
import Picture7 from "../images/cooperations/ovgu.png";
import Picture8 from "../images/cooperations/BMWK_Fz_2017_Office_Farbe_de.png";

export const Main = (props) => {
  const renderContent = () => {
    if (!props.auth && props.currentPage === "Start")
      return <LandingPage></LandingPage>;
    if (props.currentPage === "Impressum") return <Impressum></Impressum>;
    if (!props.auth && props.currentPage === "Login")
      return (
        <Login
          setCurrentPage={props.setCurrentPage}
          setAuth={props.setAuth}
          setUser={props.setUser}
        ></Login>
      );
    if (!props.auth && props.currentPage === "Signup")
      return <Signup setCurrentPage={props.setCurrentPage}></Signup>;
    if (props.auth && props.currentPage === "Setup")
      return <Setting setCurrentPage={props.setCurrentPage} user={props.user} setUser={props.setUser}></Setting>;
    if (props.auth && props.currentPage === "Start")
      return (
        <Overview
          user={props.user}
          totalRoutes={props.totalRoutes}
          setTotalRoutes={props.setTotalRoutes}
        ></Overview>
      );
    if (props.auth && props.currentPage === "Ranking")
      return <Ranking user={props.user}></Ranking>;
    if (props.currentPage === "Route planen")
      return <RoutePlanner></RoutePlanner>;
    if (props.auth && props.currentPage === "Route hinzufügen")
      return <AddRoute user={props.user}></AddRoute>;
  };
  const renderFooter = () => {
    return (
      <div style={{width: "100%", height: "200%", display: "flex", flexDirection: "column"}}>
          <div className="landingPageIconsBottomDiv">
              <div style={{backgroundImage: `url(${Picture7})`,}} className="landingPageIcons"  />
              <div style={{backgroundImage: `url(${Picture2})`,}}  className="landingPageIcons" />
              <div style={{backgroundImage: `url(${Picture1})`,}} className="landingPageIcons"  />
              <div style={{backgroundImage: `url(${Picture8})`,}} className="landingPageIcons"  />
              <div style={{backgroundImage: `url(${Picture6})`,}} className="landingPageIcons"  />
              <div style={{backgroundImage: `url(${Picture3})`,}} className="landingPageIcons"  />
              <div style={{backgroundImage: `url(${Picture4})`,}} className="landingPageIcons"  />
              <div style={{backgroundImage: `url(${Picture5})`,}} className="landingPageIcons"  />
          </div>
          <div style={{width: "100%", height: "25%"}}>
              <Link
                  onClick={() => {
                      props.setCurrentPage("Impressum");
                  }}
                  underline="none"
                  id="footerButton"
              >
                  Impressum
              </Link>
          </div>
      </div>
    );
  };
  const renderRightOuterBox = () => {
    return (
      <>
          <BrowserView style={{marginLeft: "5%", width: "90%", marginRight: "5%"}}>
              <div
              style={{
                height: "auto",
                backgroundColor: "#fadf78",
                borderRadius: "1em",
                alignItems: "center",
                display: "flex",
                flexDirection: "column",
                  marginBottom: "5%"
              }}
            >
              <p className="text" style={{ color: "black", fontSize: "1.7em" }}>
                11 Tipps zum nachhaltigen Reisen{" "}
              </p>
              <p className="text" style={{ color: "black", fontSize: "1em" }}>
                <em>
                  Lerne mit Hilfe von 11 Tipps deine Reisen in Zukunft nachhaltiger
                  zu gestalten
                </em>
              </p>
              <Button
                onClick={() => {
                  window.open("https://reisevergnuegen.com/nachhaltig-reisen/");
                }}
                style={{
                  backgroundColor: "#3E3B3BFF",
                  color: "white",
                  borderRadius: "0.7em",
                  minWidth: "40%",
                  height: "20%",
                  fontFamily: "'Lato', sans-serif",
                  fontSize: 20,
                  marginBottom: "2.5%",
                  marginTop: "2.5%",
                }}
              >
                Tipps
              </Button>
            </div>
            <div
              style={{
                width: "100%",
                height: "auto",
                backgroundColor: "#fadf78",
                borderRadius: "1em",
                alignItems: "center",
                display: "flex",
                flexDirection: "column",
              }}
            >
              <p className="text" style={{ color: "black", fontSize: "1.7em" }}>
                Nachhaltigkeit in Sachsen-Anhalt
              </p>
              <p className="text" style={{ color: "black", fontSize: "1em" }}>
                <em>
                  Finde regionale Einrichtungen, Organisation und Betriebe, die
                  nachhaltig ausgerichtet sind
                </em>
              </p>
              <Button
                onClick={() => {
                  window.open(
                    "https://www.kosa21.de/projekte-entdecken/sachsen-anhalt-nachhaltig"
                  );
                }}
                style={{
                  backgroundColor: "#3E3B3BFF",
                  color: "white",
                  borderRadius: "0.7em",
                  width: "auto",
                  height: "20%",
                  fontFamily: "'Lato', sans-serif",
                  fontSize: 20,
                  marginBottom: "2.5%",
                  marginTop: "2.5%",
                }}
              >
                Anregungen
              </Button>
            </div>
          </BrowserView>
      </>
    );
  };

  return (
    <div className="mainDiv">
      <div
        className="outerDiv"
        style={{
          backgroundImage: `url(${background})`,
          backgroundRepeat: "no-repeat",
          backgroundSize: "cover",
        }}
      ></div>
      
      <div className="centralDiv">
        <div className="contentDiv">{renderContent()}</div>
        <div className="footerDiv">{renderFooter()}</div>  
      </div>
      <div
        className="outerDiv"
        style={{
          backgroundImage: `url(${background})`,
          backgroundRepeat: "no-repeat",
          backgroundSize: "cover",
          verticalAlign: "center",
        }}
      >
        {renderRightOuterBox()}
      </div>
    </div>
  );
};
