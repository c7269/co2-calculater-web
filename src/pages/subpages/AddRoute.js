import React, { useRef, useState } from "react";
import DirectionsCarIcon from "@mui/icons-material/DirectionsCar";
import DirectionsSubwayIcon from "@mui/icons-material/DirectionsSubway";
import DirectionsBusIcon from "@mui/icons-material/DirectionsBus";
import DirectionsBikeIcon from "@mui/icons-material/DirectionsBike";
import {BrowserView} from 'react-device-detect';
import { makeStyles } from "@mui/styles";
import {
  Button,
  FormControlLabel,
  FormLabel,
  Radio,
  RadioGroup,
  Select,
} from "@mui/material";
import MenuItem from "@mui/material/MenuItem";
import UploadFileIcon from "@mui/icons-material/UploadFile";
import FormControl from "@mui/material/FormControl";
import SimpleDialog from "./Dialog";
import "../../styles/standard.css";
import "../../styles/addRoute.css";
import PositionAutocomplete from "../../components/PositionAutocomplete";
import axios from "axios";
const useStyles = makeStyles(() => ({
  height: {
    height: "155px",
    ['@media only screen and (max-device-width: 480px)']: {
      height: "45px"
    },
  },

}))

export const AddRoute = (props) => {
  const classes = useStyles();
  const inputRef = useRef();
  const [fileButton, setFileButton] = useState("Nachweis hochladen...");

  const [clicked, setClicked] = useState(false);
  const [startPoint, setStartPoint] = useState("");
  const [endPoint, setEndPoint] = useState("");
  const [co2Emission, setco2Emission] = useState();
  const [selectedTransportation, setSelectedTransportation] = useState("");
  const [selectedCarClass, setSelectedCarClass] = useState("");
  const [validation, setValidation] = useState(false);
  const [consumption, setConsumption] = useState();
  const [fuel, setFuel] = useState(false);
  const [loading, isLoading] = useState(true);

  const transportation = [
    {
      Name: "Auto",
      Icon: DirectionsCarIcon,
    },
    {
      Name: "Zug",
      Icon: DirectionsSubwayIcon,
    },
    {
      Name: "Bus",
      Icon: DirectionsBusIcon,
    },
    {
      Name: "Fahrrad",
      Icon: DirectionsBikeIcon,
    },
  ];

  const calcEmission = (distance) => {
    var compareValue = (distance * 150) / 1000;
    compareValue = Math.round((compareValue + Number.EPSILON) * 100) / 100;
    if (selectedTransportation === "Auto") {
      setco2Emission(compareValue);
      return [compareValue, -compareValue];
    }
    if (selectedTransportation === "Fahrrad") {
      setco2Emission(0);
      return [0, compareValue];
    }
    if (selectedTransportation === "Bahn") {
      setco2Emission(
        Math.round(((distance * 40) / 1000 + Number.EPSILON) * 100) / 100
      );
      return [
        Math.round(((distance * 40) / 1000 + Number.EPSILON) * 100) / 100,
        Math.round(((distance * 40) / 1000 + Number.EPSILON) * 100) / 100 -
          compareValue,
      ];
    }
    if (selectedTransportation === "Bus") {
      setco2Emission(
        Math.round(((distance * 20) / 1000 + Number.EPSILON) * 100) / 100
      );
      return [
        Math.round(((distance * 20) / 1000 + Number.EPSILON) * 100) / 100,
        Math.round(((distance * 20) / 1000 + Number.EPSILON) * 100) / 100 -
          compareValue,
      ];
    }
  };

  const handleClick = async () => {
    if (startPoint == "" || endPoint == "" || selectedTransportation == "")
      setClicked(false);
    if (
      selectedTransportation == "Auto" &&
      consumption == null &&
      typeof consumption != "number"
    )
      setClicked(false);
    else {
      var pos1 = await axios.get(
        `https://geocoder.ls.hereapi.com/6.2/geocode.json?apiKey=EEAnGX6cMH5_ScGwLQqU40IYYGacZDojipUmbpVCHdg&jsonattributes=1&gen=9&locationid=` +
          startPoint.locationId
      );
      var pos2 = await axios.get(
        `https://geocoder.ls.hereapi.com/6.2/geocode.json?apiKey=EEAnGX6cMH5_ScGwLQqU40IYYGacZDojipUmbpVCHdg&jsonattributes=1&gen=9&locationid=` +
          endPoint.locationId
      );
      var pos1Data = {
        latitude:
          pos1.data.response.view[0].result[0].location.displayPosition
            .latitude,
        longitude:
          pos1.data.response.view[0].result[0].location.displayPosition
            .longitude,
      };
      var pos2Data = {
        latitude:
          pos2.data.response.view[0].result[0].location.displayPosition
            .latitude,
        longitude:
          pos2.data.response.view[0].result[0].location.displayPosition
            .longitude,
      };

      var km = await axios.get(
        "https://router.project-osrm.org/route/v1/driving/" +
          pos1Data.longitude +
          "," +
          pos1Data.latitude +
          ";" +
          pos2Data.longitude +
          "," +
          pos2Data.latitude
      );
      km = km.data.routes[0];
      var distance = km.distance / 1000; // in kilometers
      var durationCar = km.duration / 60;
      var co2 = calcEmission(distance);

      axios
        .post("/addRoute", {
          user: props.user,
          start: startPoint.label,
          end: endPoint.label,
          transportationType: selectedTransportation,
          validation: validation,
          co2Emission: co2[0],
          rankingValue: co2[1],
        })
        .then((res) => {
          console.log(res);
        })
        .catch((error) => alert(error));
      setClicked(true);
      isLoading(false);
    }
  };
  const returnToInput = () => {
    setClicked(false);
    setStartPoint("");
    setEndPoint("");
    setSelectedTransportation("");
    setSelectedCarClass("");
    setConsumption(null);
  };

  const calcConsumption = (e) => {
    if (e[1] === "Diesel") {
      setConsumption(Number(e[0].consumptionDiesel));
      setFuel(true);
    } else {
      setConsumption(Number(e[0].consumptionBenzin));
      setFuel(false);
    }
  };
  const renderTransportation = () => {
    const list = [];
    transportation.forEach((element) => {
      list.push(
        <MenuItem value={element.Name}>
          <div style={{ width: "100%", height: "100%", fontSize: 20 }}>
            <div
              style={{
                width: "20%",
                float: "left",
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <element.Icon style={{ fontSize: 25 }} />
            </div>
            {element.Name}
          </div>
        </MenuItem>
      );
    });
    return list;
  };
  return (
    <div style={{ height: "90%", width: "100%" }}>
      {clicked ? (
        <>
          <div className="headLine">Route hinzufügen</div>
          <div
            style={{
              width: "100%",
              display: "flex",
              justifyContent: "left",
              flexDirection: "column",
              marginLeft: 25,
            }}
          >
            <div
              style={{
                marginLeft: "5%",
                fontSize: 20,
                color: "white",
                marginBottom: "30px",
              }}
            >
              <em>Route hinzugefügt!</em>
            </div>
            <div
              className="routeSubHeadline"
              style={{ marginTop: "0", marginBottom: 0, lineHeight: "1.4em" }}
            >
              Auf deiner gewählten Reise von <br />
              <b className="routeSubHeadlineBold"> {startPoint.label}</b>
              <br />
              nach
              <br />
              <b className="routeSubHeadlineBold">{endPoint.label}</b>
              <br />
              hast du
              <div
                style={{
                  fontSize: 50,
                  marginTop: 10,
                  marginLeft: 10,
                  color: "",
                }}
              >
                {co2Emission} kg C0<sub>2</sub>
              </div>
              verbraucht.
            </div>
            <div className="textLeftAligned">
              <em>
                Um dem Klimawandel entgegen zu wirken, sollte eine Person
                maximal 0.600 t C0<sub>2</sub> pro Jahr verbrauchen.
              </em>
              <div style={{ marginTop: 0, lineHeight: "1.4em" }}>
                  {" "}
                  Diese Route wirkt sich {validation ? "" : "nicht"} auf deinen
                  Score aus. Um diesen wieder anzuheben, wähle zukünftig nachhaltigere
                Verkehrsmittel oder spende Bäume, um die verbrauchten Emissionen
                  auszugleichen.{" "}
              </div>
            </div>
          </div>
          <div
            style={{
              width: "100%",
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              flexDirection: "column",
              marginTop: 10,
            }}
          >
            <Button
              style={{
                width: "40%",
                height: 50,
                marginBottom: 10,
                backgroundColor: "green",
              }}
              variant="contained"
              id="buttonLogin"
              onClick={() => {
                returnToInput();
                window.open(
                  "https://www.tourismusnetzwerk-sachsen-anhalt.de/de/gruene-karte.html",
                  "_blank"
                );
              }}
            >
              Bäume pflanzen
            </Button>
            <input
              type="submit"
              value="Weitere Fahrt hinzufügen"
              style={{
                width: "40%",
                height: "50px",
                borderRadius: "20px",
                fontSize: 22,
                outline: "none",
                border: 0,
                backgroundColor: "#fff",
                color: "black",
                cursor: "pointer",
              }}
              onClick={() => {
                returnToInput();
              }}
            ></input>
          </div>
        </>
      ) : (
        <>
          <div className="headLine">Route hinzufügen</div>
          <div className="textLeftAligned">
            <em>
              Gib das Fortbewegungsmittel, Beginn und Ziel deiner zu planenden
              Reise an. Der Nachweis ist notwendig, um die Route zum Ranking hinzuzufügen.
            </em>
          </div>
          <div style={{width: "100%"}}>
            <table
                style={{
                  borderCollapse: "separate",
                  borderSpacing: "1em",
                  width: "100%",
                  tableLayout: "fixed"
                }}
            >
              <tr>
                <td className="routeFirstColumn">Start</td>
                <td className="autoCompleteDiv">
                  <PositionAutocomplete
                      setSelectedOption={setStartPoint}
                  />
                </td>
                <td className="autoCompleteFiller" />
              </tr>
              <tr>
                <td className="routeFirstColumn">Ziel</td>
                <td className="autoCompleteDiv">
                  <PositionAutocomplete
                      setSelectedOption={setEndPoint}
                      height={"55px"}
                      additionalStyles={{ height: "55px" }}
                  />
                </td>
                <td className="autoCompleteFiller" />
              </tr>
              <tr>
                <td className="routeFirstColumn">Verkehrsmittel</td>
                <td className="autoCompleteDiv">
                  <Select
                      className="uploadRoute"
                      style={{borderRadius: "0.75em"}}
                      value={selectedTransportation}
                      onChange={(e) => {
                        setSelectedTransportation(e.target.value);
                      }}
                  >
                    {renderTransportation()}
                  </Select>
                </td>
                <td className="autoCompleteFiller" />
              </tr>
              {selectedTransportation !== "Auto" ? (
                  ""
              ) : (
                  <>
                    <tr>
                      <td
                          className="routeFirstColumnSmall"
                          style={{ textAlign: "center", width: "20%" }}
                      >
                        Kraftstoffart
                      </td>
                      <td className="autoCompleteDiv">
                        <FormControl>
                          <RadioGroup
                              row
                              defaultValue={false}
                              name="radio-buttons-group"
                              style={{ color: "black", width: "100%" }}
                              onChange={(e) => {
                                setFuel(e.target.value);
                              }}
                              value={fuel}
                          >
                            <FormControlLabel
                                value={false}
                                control={
                                  <Radio
                                      sx={{
                                        "&.Mui-checked": {
                                          color: "#d32b2b",
                                        },
                                      }}
                                  />
                                }
                                label="Benzin"
                            />
                            <FormControlLabel
                                value={true}
                                control={
                                  <Radio
                                      sx={{
                                        "&.Mui-checked": {
                                          color: "#d32b2b",
                                        },
                                      }}
                                  />
                                }
                                label="Diesel"
                            />
                          </RadioGroup>
                        </FormControl>
                      </td>
                      <td className="autoCompleteFiller" />
                    </tr>
                    <tr>
                      <td
                          className="routeFirstColumnSmall"
                          style={{ textAlign: "center", width: "20%" }}
                      >
                        Verbrauch (l/km)
                      </td>
                      <td className="autoCompleteDiv">
                        <input
                            type="number"
                            value={consumption}
                            onInput={(e) => setConsumption(Number(e.target.value))}
                            style={{
                              backgroundColor: "#fff",
                              width: "100%",
                              height: "35px",
                              borderRadius: "10px",
                              fontSize: 22,
                              outline: "none !important",
                              border: 0,
                              justifyContent: "center",
                            }}
                        />
                      </td>
                      <td className="autoCompleteFiller">
                        <BrowserView>
                          <SimpleDialog
                              passCarClass={(e) => {
                                setSelectedCarClass(e[0]);
                                calcConsumption(e);
                              }}
                          />
                        </BrowserView>
                      </td>
                    </tr>
                  </>
              )}
              <tr>
                <td className="routeFirstColumn">
                  Nachweis
                  <div style={{ fontSize: "0.7em" }}>(optional)</div>
                </td>
                <td className="autoCompleteDiv">
                  <button
                      className="uploadRoute"
                      onClick={() => {
                        inputRef.current.click();
                      }}
                  >
                    <em>{fileButton}</em>
                    <UploadFileIcon style={{ float: "right", fontSize: "1.5em" }} />
                  </button>
                  <input
                      onChange={() => {
                        setValidation(true);
                        setFileButton(inputRef.current.files[0].name);
                      }}
                      type="file"
                      ref={inputRef}
                      style={{ visibility: "hidden" }}
                  />
                </td>
              </tr>
              <tr>
                <td className="routeFirstColumn"/>
                <td className="autoCompleteDiv">
                  <Button
                      id="buttonLogin"
                      style={{
                        width: "100%",
                        height: "55px",
                        cursor: "pointer",
                      }}
                      onClick={() => {
                        handleClick();
                      }}
                  >
                    Berechnen
                  </Button>
                </td>
                <td className="autoCompleteFiller" />
              </tr>
            </table>
          </div>

        </>
      )}
    </div>
  );
};
