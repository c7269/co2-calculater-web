import * as React from 'react';
import PropTypes from 'prop-types';
import DialogTitle from '@mui/material/DialogTitle';
import Dialog from '@mui/material/Dialog';
import kompaktwagen from "../../images/Kompaktwagen.jpg";
import mittelklassewagen from "../../images/Mittelklassewagen.jpg";
import SUV from "../../images/SUV.jpg";
import transporter from "../../images/Transporter.jpg"
import {IconButton, ImageList, ImageListItem, ImageListItemBar, ListSubheader} from "@mui/material";

const images = [

    {
        name: "Kompaktklasse",
        img: kompaktwagen,
        consumptionBenzin: 8,
        consumptionDiesel: 5
    },
    {
        name: "Mittelklasse",
        img: mittelklassewagen,
        consumptionBenzin: 10,
        consumptionDiesel: 8,
    },
    {
        name: "SUV",
        img: SUV,
        consumptionBenzin: 11.5,
        consumptionDiesel: 12.5
    },
    {
        name: "Transporter",
        img: transporter,
        consumptionDiesel: 13,
        consumptionBenzin: 14
    }
];
function _Dialog(props) {
    const { onClose, selectedValue, open } = props;

    const handleClose = () => {
        onClose(selectedValue);
    };

    const handleListItemClick = (value) => {
        onClose(value);
    };
    const showTitle = (type, name) => {
        return <>
            <div>{name}</div><div>{type}</div>
        </>
    }

    return (
        <Dialog
            onClose={handleClose}
            open={open}
        >
            <DialogTitle style={{color: "#707070", textAlign: "center"}}>Wählen Sie Ihre Fahrzeugklasse aus</DialogTitle>
            <ImageList>
                <ImageListItem key="Subheader" cols={2}>
                </ImageListItem>
                {images.map((image) => (
                    <ImageListItem key={image.name}>
                    >
                        <img
                            src={`${image.img}?w=200&fit=crop&auto=format`}
                            srcSet={`${image.img}?w=200&fit=crop&auto=format&dpr=2 2x`}
                            alt={image.name}
                            loading="lazy"
                        />
                        <div>
                            <ImageListItemBar
                                style={{textAlign: "center", width: "50%", backgroundColor: "rgb(61,61,61, 0.7)", cursor: "pointer"}}
                                title={showTitle("Benzin", image.name)}
                                onClick={() => {handleListItemClick([image, "Benzin"])
                                    console.log(image.name)}}
                            />
                            <ImageListItemBar
                                style={{textAlign: "center", width: "50%", marginLeft: "50%", backgroundColor: "707070FF", cursor: "pointer"}}
                                title={showTitle("Diesel", image.name)}
                                onClick={() => {handleListItemClick([image, "Diesel"])
                                    console.log(image.name)}}
                            />
                        </div>
                    </ImageListItem>
                ))}
            </ImageList>
        </Dialog>
    );
}

_Dialog.propTypes = {
    onClose: PropTypes.func.isRequired,
    open: PropTypes.bool.isRequired,
    selectedValue: PropTypes.string.isRequired,
};

export default function SimpleDialog(props) {
    const [open, setOpen] = React.useState(false);
    const [selectedValue, setSelectedValue] = React.useState(images[0]);

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = (value) => {
        setOpen(false);
        setSelectedValue(value);
        props.passCarClass(value)
    };

    return (
        <div>
            <button onClick={handleClickOpen} style={{backgroundColor:"#fff", width: "100%",height:"35px",borderRadius:"10px", fontSize: 12,outline:"none !important", border: 0, justifyContent: "center", cursor: "pointer", fontFamily: "'Lato', sans-serif"}}>Ich kenne meinen Verbrauch nicht</button>
            <_Dialog
                selectedValue={selectedValue}
                open={open}
                onClose={handleClose}
            />
        </div>
    );
}
