import {Button} from "@mui/material";
import "../../styles/standard.css"
export const Impressum = () => {
    return <>
        <div style={{ height: "100%", width: "100%" }}>
            <div className="headLine" style={{maxHeight: "20%"}}>
                Impressum
            </div>
            <div style={{marginLeft: "5%", width: "95%"}}>
            <div className="impressumText">
                Angaben gemäß § 5 TMG:
                <p style={{paddingLeft: 2, margin: "0.25%", fontSize: "0.7em"}}>
                    Stiftung Umwelt, Natur- und Klimaschutz des Landes Sachsen-Anhalt (SUNK)<br></br>
                    Steubenallee 2<br></br>
                    39104 Magdeburg
                </p>

            </div>
            <div className="impressumText">
                Vertreten durch:
                <p style={{paddingLeft: 2, margin: "0.25%", fontSize: "0.7em"}}>
                    Geschäftsführerin Dr. Nele Herkt
                </p>
            </div>
            <div className="impressumText">
                Kontakt:
                <p style={{paddingLeft: 2, margin: "0.25%", fontSize: "0.7em"}}>
                    Telefon: 0391 556866-10<br></br>
                    Telefax: 0391 556866-66<br></br>
                    E-Mail: info(at)sunk-lsa.de
                </p>
            </div>
            <div className="impressumText">
                Aufsichtsbehörde:
                <p style={{paddingLeft: 2, margin: "0.25%", fontSize: "0.7em"}}>
                    Ministerium für Wissenschaft, Energie, Klimaschutz und Umwelt<br></br>
                    Leipziger Str. 58<br></br>
                    39112 Magdeburg<br></br>
                    https://mwu.sachsen-anhalt.de/
                </p>
            </div>
            <div className="impressumText">
                Verantwortlich für den Inhalt nach § 55 Abs. 2 RStV:
                <p style={{paddingLeft: 2, margin: "0.25%", fontSize: "0.7em"}}>
                    Stiftung Umwelt, Natur- und Klimaschutz des Landes Sachsen-Anhalt<br></br>
                    Steubenallee 2<br></br>
                    39104 Magdeburg
                </p>
            </div>
            <div className="impressumText">
                Haftung für Inhalte
                <p style={{paddingLeft: 2, margin: "0.25%", fontSize: "0.7em"}}>
                    Als Diensteanbieter sind wir gemäß § 7 Abs. 1 TMG TTBSG für eigene Inhalte auf diesen Seiten nach den allgemeinen Gesetzen verantwortlich. Nach §§ 8 bis 10 TMG sind wir als Diensteanbieter jedoch nicht verpflichtet, übermittelte oder gespeicherte fremde Informationen zu überwachen oder nach Umständen zu forschen, die auf eine rechtswidrige Tätigkeit hinweisen. <br></br>
                    Verpflichtungen zur Entfernung oder Sperrung der Nutzung von Informationen nach den allgemeinen Gesetzen bleiben hiervon unberührt. Eine diesbezügliche Haftung ist jedoch erst ab dem Zeitpunkt der Kenntnis einer konkreten Rechtsverletzung möglich. Bei Bekanntwerden von entsprechenden Rechtsverletzungen werden wir diese Inhalte umgehend entfernen.
                </p>
               </div>
            <div className="impressumText">
                Haftung für Links
                <p style={{paddingLeft: 2, margin: "0.25%", fontSize: "0.7em"}}>
                    Unser Angebot enthält Links zu externen Websites Dritter, auf deren Inhalte wir keinen Einfluss haben. Deshalb können wir für diese fremden Inhalte auch keine Gewähr übernehmen. Für die Inhalte der verlinkten Seiten ist stets der jeweilige Anbieter oder Betreiber der Seiten verantwortlich. Die verlinkten Seiten wurden zum Zeitpunkt der Verlinkung auf mögliche Rechtsverstöße überprüft. Rechtswidrige Inhalte waren zum Zeitpunkt der Verlinkung nicht erkennbar.
                    Eine permanente inhaltliche Kontrolle der verlinkten Seiten ist jedoch ohne konkrete Anhaltspunkte einer Rechtsverletzung nicht zumutbar. Bei Bekanntwerden von Rechtsverletzungen werden wir derartige Links umgehend entfernen.<br></br>
                </p>
               </div>
            <div className="impressumText">
                Urheberrecht
                <p style={{paddingLeft: 2, margin: "0.25%", fontSize: "0.7em"}}>
                    Die durch die Seitenbetreiber erstellten Inhalte und Werke auf diesen Seiten unterliegen dem deutschen Urheberrecht. Die Vervielfältigung, Bearbeitung, Verbreitung und jede Art der Verwertung außerhalb der Grenzen des Urheberrechtes bedürfen der schriftlichen Zustimmung des jeweiligen Autors bzw. Erstellers. Downloads und Kopien dieser Seite sind nur für den privaten, nicht kommerziellen Gebrauch gestattet.<br></br>
                    Soweit die Inhalte auf dieser Seite nicht vom Betreiber erstellt wurden, werden die Urheberrechte Dritter beachtet. Insbesondere werden Inhalte Dritter als solche gekennzeichnet. Sollten Sie trotzdem auf eine Urheberrechtsverletzung aufmerksam werden, bitten wir um einen entsprechenden Hinweis. Bei Bekanntwerden von Rechtsverletzungen werden wir derartige Inhalte umgehend entfernen.
                </p>
               </div>
        </div>
            </div>

    </>
}