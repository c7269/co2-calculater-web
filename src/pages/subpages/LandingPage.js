import Cooperation from "../../images/cooperations/Logo-LTV.png"
import OVGU from "../../images/cooperations/ovgu.png"
import "../../styles/landingPage.css"



export const LandingPage = ()=>{
    return <>
        <div style={{width: "100%", height: "100%"}}>
            <div className="headlineLandingPage">
                Herzlich Willkommen zum CO<sub>2</sub>-Rechner des LTV Sachsen-Anhalt
            </div>
            <div style={{marginLeft: "5%", marginRight: "5%", width: "90%", height: "30%", flexDirection: "row", display: "flex"}}>
                <div className="logoLandingPage" style={{backgroundImage: `url(${Cooperation})`}} onClick={() =>  {window.open("https://www.sunk-lsa.de/gruene-karte-zeigen", "_blank")}} />
                <div className="textLandingPage">in Kooperation mit</div>
                <div className="logoLandingPage" style={{backgroundImage: `url(${OVGU})`}} onClick={() =>  {window.open("https://cse.cs.ovgu.de/cse/", "_blank")}} />
            </div>
        </div>
    </>
}