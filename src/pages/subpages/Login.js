import { Button } from "@mui/material"
import "../../styles/standard.css"
import "../../styles/login.css"
import {useState} from "react";
import axios from "axios";

export const Login = (props) => {
    const [tmp, setTmp] = useState(undefined)
    const [id, setID] = useState(undefined)
    const [email, setEmail] = useState(undefined)
    const [password, setPassword] = useState(undefined)
    const handleSignUp = () => {
        axios.post('/login', {id: tmp, password})
            .then((res) => {
                res = res.data.user
                props.setAuth(true)
                const user = { id: res.id, email: res.email, password: res.password, co2Emission: res.co2Emission };
                // set the state of the user
                // store the user in localStorage
                localStorage.setItem('user', JSON.stringify(user))
                props.setUser(user)
                props.setCurrentPage("Start")
            })
            .catch(error => alert('Login failed'))
    }
    return <>
        <div style={{ height: "100%", width: "100%" }}>
            <div className="headLine">
                Anmelden
            </div>
            <div className="divContainerSignUp">
                <div style={{height:"100%",width:"30%",display:"flex",justifyContent:"center",alignItems:"center",flexDirection:"column",textAlign:"center",color:"black"}}>
                    <label className="labelSignUp">ID</label>
                    <label className="labelSignUp">Passwort</label>
                </div>
                <div style={{height:"100%",width:"70%",display:"flex",justifyContent:"center",alignItems:"center",flexDirection:"column"}}>
                    <input type="text" className="inputSignUp" input={tmp} onInput={e => setTmp((e.target.value))} placeholder="E-Mail-Adresse / ID"></input>
                    <input type="password" className="inputSignUp" input={password} onInput={e => setPassword((e.target.value))} onKeyPress={e => { if (e.key === "Enter"){handleSignUp()}}}></input>
                </div>
            </div>
            <div style={{width:"90%",display:"flex",flexDirection:"column", marginLeft: "5%", marginRight: "5%"}}>
                <div style={{width: "70%", marginLeft: "35%"}}>
                    <Button style={{width: "85%"}} variant="contained" id="buttonLogin" onClick={() => {handleSignUp()}}>Einloggen</Button>
                    <div className="textLeftAligned" style={{margin: 0, display: "flex",textAlign: "center", justifyContent:"center",alignItems:"center", width: "85%", marginTop: "2.5%", flexDirection: "column"}}>
                        Du hast noch keinen Account? <br></br>
                        Dann kannst du dich jetzt kostenlos registrieren
                        <Button variant="text" style={{alignItems: "center", display: "flex", color: "black", justifyContent: "center"}}
                                onClick={() => {props.setCurrentPage("Signup")}}
                        >Registrieren</Button>
                    </div>
                </div>
            </div>
        </div>

    </>
}