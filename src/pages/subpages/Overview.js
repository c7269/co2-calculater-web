import FormControl from '@mui/material/FormControl';
import { useTheme } from '@mui/material/styles';
import OutlinedInput from '@mui/material/OutlinedInput';
import MenuItem from '@mui/material/MenuItem';
import Select from '@mui/material/Select';
import * as React from 'react';
import "../../styles/standard.css"
import "../../styles/overview.css"
import highCO2 from "../../images/tankneedles/03.png"
import neutralCO2 from "../../images/tankneedles/02.png"
import lowCO2 from "../../images/tankneedles/01.png"
import {Button} from "@mui/material";
import {useEffect} from "react";
import axios from "axios";
import Spenden from "../../images/Spenden.png"
import {BrowserView, MobileView} from 'react-device-detect';

export const Overview = (props) => {

    useEffect(() => {
        axios.post('/getChronic', {id: props.user.id})
            .then((res) => {
                props.setTotalRoutes(res.data.routes)
            })
            .catch(error => console.log(error))
    }, []);

    const ITEM_HEIGHT = 100;
    const ITEM_PADDING_TOP = 8;
    const MenuProps = {
        PaperProps: {
            style: {
                maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
                width: 250,
            },
        },
    };


    function getStyles(name, personName, theme) {
        return {
            fontWeight:
                personName.indexOf(name) === -1
                    ? theme.typography.fontWeightRegular
                    : theme.typography.fontWeightMedium,
        };
    }

    const theme = useTheme();
    const [personName, setPersonName] = React.useState([]);

    const handleChange = (event) => {
        const {
            target: { value },
        } = event;
        setPersonName(
            // On autofill we get a stringified value.
            typeof value === 'string' ? value.split(',') : value,
        );
    };
    return <>
        <div style={{ height: "100%", width: "100%" }}>
            <div style={{width: "100%", flexDirection: "row", height: "20%"}}>
                <div className="headLine" style={{width: "45%", float:"left"}}>
                    Hallo
                    {props.user.id}!
                </div>
                <div className="tankNeedle" style={{backgroundImage: props.user.co2Emission > 0 ? `url(${highCO2})`: props.user.co2Emission === 0 ? `url(${neutralCO2})` : `url(${lowCO2})`,}}>
                </div>
            </div>
            <div style={{ width: "100%", display: "flex", flexDirection: "column", height: "25%", fontSize: "1.5em" }}>
                <div className="textLeftAligned" style={props.user.co2Emission > 0 ? {color: "#eb0000"} : props.user.co2Emission === 0 ? {color: "#4c4c4c"} : {color: "#306c2f"}}>
                    Deine aktuelle CO<sub>2</sub>-Bilanz ist {props.user.co2Emission > 0 ? "negativ" : props.user.co2Emission == 0 ? "neutral": "positiv"}.
                </div>
                <div className="chronic">
                    <FormControl sx={{ m: 1, width: "100%", mt: 3}} style={{backgroundColor:"#fff",fontSize:33, margin: 0}}>
                        <Select
                            displayEmpty
                            value={personName}
                            onChange={handleChange}
                            input={<OutlinedInput />}
                            renderValue={(selected) => {
                                if (selected.length === 0) {
                                    return <em>Chronik</em>;
                                }

                                return selected.join(', ');
                            }}
                            MenuProps={MenuProps}
                            inputProps={{ 'aria-label': 'Without label',height:"100px",fontSize:33 }}
                        >
                            {props.totalRoutes.length === 0 ?
                                <MenuItem disabled value="" style={{width: "100%"}}>
                                    <em style={{width: "100%", fontFamily: "'Lato', sans-serif"}}>Es wurden noch keine Routen eingetragen</em>
                                </MenuItem> :
                                <MenuItem disabled value="" style={{width: "100%"}}>
                                    <tr className="chronicTR">
                                        <td style={{width: "20%", height: "auto", wordWrap: "break-word"}}>
                                            Verkehrsmittel
                                        </td>
                                        <td style={{width: "32.5%"}}>
                                            Start
                                        </td>
                                        <td style={{width: "30%", height: "auto"}}>
                                            Ziel
                                        </td>
                                        <td style={{width: "17.5%", height: "auto"}}>
                                            CO<sub>2</sub>-Emission
                                        </td>
                                    </tr>
                                </MenuItem>
                            }

                            {props.totalRoutes.map((name) => (
                                <MenuItem
                                    disabled
                                    style={{opacity: 1}}
                                    key={name._id}
                                    value={name.start + " -> " + name.end}
                                >
                                    <tr className="chronicTR">
                                            <td style={{width: "20%", whiteSpace: "normal"}}>
                                                {name.transportationType}
                                            </td>
                                            <td style={{width: "30%",marginRight: "2.5%", whiteSpace: "normal"}}>
                                                {name.start}
                                            </td>
                                            <td style={{width: "30%", height: "auto", whiteSpace: "normal"}}>
                                                {name.end}
                                            </td>
                                            <td style={{width: "17.5%", height: "auto"}}>
                                                {name.co2Emission} kg
                                            </td>
                                    </tr>
                                </MenuItem>
                            ))}
                        </Select>
                    </FormControl>
                </div>
            </div>
            <div style={{height: "35%"}}>
            <BrowserView style={{width: "100%", height: "100%"}}>
                    <Button style={{backgroundImage: `url(${Spenden})`, backgroundPosition: "center", backgroundRepeat: "no-repeat", backgroundSize: "contain",marginRight: "10%", marginBottom:"25%",width: "30%", height: "85%", float: "right"}}
                            onClick={() => {window.open("https://www.sunk-lsa.de/gruene-karte-zeigen", "_blank")}}
                    >
                    </Button>
                    <div className="textLeftAligned" style={{fontSize: "1.7em"}}>Verbessere deine CO<sub>2</sub>-Bilanz, indem du Bäume spendest!</div>
            </BrowserView>
            <MobileView style={{width: "100%", height: "85%"}}>
                    <div className="textLeftAligned" style={{fontSize: "1.3em"}}>Verbessere deine CO<sub>2</sub>-Bilanz, indem du Bäume spendest!</div>
                    <Button style={{backgroundImage: `url(${Spenden})`, backgroundPosition: "center", backgroundRepeat: "no-repeat", backgroundSize: "auto 100%",width: "100%", height: "80%"}}
                            onClick={() => {window.open("https://www.sunk-lsa.de/gruene-karte-zeigen", "_blank")}}
                    >
                    </Button>
            </MobileView>
            </div>
        </div>

    </>
}