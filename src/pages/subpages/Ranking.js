import "../../styles/standard.css"
import {useEffect, useState} from "react";
import axios from "axios";
import {Pagination, PaginationItem} from "@mui/material";
import "../../styles/ranking.css"
import {
    DataGrid,
    gridPageCountSelector,
    gridPageSelector,
    useGridApiContext,
    useGridSelector,
} from '@mui/x-data-grid';
import {makeStyles} from "@mui/styles";
import {logDOM} from "@testing-library/react";

const useStyles = makeStyles(() => ({
    ul: {
        "& .MuiPaginationItem-root": {
            color: "#fff",
            borderColor: "white",
        },
        marginRight: 10
    },
    root: {
        fontFamily: "'Lato', sans-serif",
        color: "white",
        fontSize: "1.2em",
        ['@media only screen and (max-device-width: 480px)']: {
            fontSize: "0.5em"
        },
        border: 0,
        borderRight: 0,
        '&.MuiDataGrid-root .MuiDataGrid-cell:focus': {
            outline: 'none',
        },
        '&.MuiDataGrid-root .MuiDataGrid-columnHeader:focus': {
            outline: 'none',
        },
        '& .MuiDataGrid-columnsContainer, .MuiDataGrid-cell': {
            borderBottom: `1px solid #f0f0f0`,
        },
        '& .MuiPaginationItem-root': {
            borderRadius: 0,
        },
        '& .MuiDataGrid-iconSeparator': {
            display: 'none',
        },
        '& .MuiSvgIcon-root': {
            color: "white"
        },

    }
}));
export const Ranking = (props) => {
    const classes = useStyles();
    const [position, setPosition] = useState(undefined)
    const [ranking, setRanking] = useState([])
    useEffect(async () => {
        await axios.post('/getLeaderboard')
            .then((res) => {
                var tmp = []
                for (var i = 0; i < res.data.users.length; i++) {
                    tmp.push({
                        position: i + 1 + ".",
                        id: res.data.users[i].id,
                        rankingValue: res.data.users[i].rankingValue
                    })
                    if (res.data.users[i].id === props.user.id){
                        setPosition(i+1)
                    }
                }
                setRanking(tmp)
            })
            .catch(error => console.log(error))
    }, []);
    const columns = [
        {
            field: 'position',
            headerName: 'Position',
            flex: 1,
            headerAlign: "center",
            align: "center",
        },
        {
            field: 'id',
            headerName: 'ID',
            flex: 1,
            headerAlign: "center",
            align: "center",
            type: "string",
        },
        {
            field: 'rankingValue',
            headerName: 'Punkte',
            flex: 1,
            headerAlign: "center",
            align: "center",
            type: "number",

        },
    ];
    function CustomPagination() {
        const apiRef = useGridApiContext();
        const page = useGridSelector(apiRef, gridPageSelector);
        const pageCount = useGridSelector(apiRef, gridPageCountSelector);

        return (
            <Pagination
                variant="outlined"
                classes={{ul: classes.ul}}
                shape="rounded"
                page={page + 1}
                count={pageCount}
                renderItem={(props2) => <PaginationItem {...props2} disableRipple />}
                onChange={(event, value) => apiRef.current.setPage(value - 1)}
            />
        );
    }

    const renderRanking = () => {
        return <>
                <DataGrid
                    sx={{
                        '& .highlight.selected': {
                            backgroundColor: 'rgba(176,176,176,0.8)',
                        },
                    }}
                    className={classes.root}
                    rows={ranking}
                    columns={columns}
                    pageSize={4}
                    disableColumnSelector
                    disableColumnMenu
                    isRowSelectable={() => false}
                    components={{
                        Pagination: CustomPagination,
                    }}
                    getRowClassName={(params) => params.row.id === props.user.id ? 'highlight selected' : params.row.id}
                />
        </>
    }
    return <>
        <div style={{ height: "100%", width: "100%" }}>
            <div className="headLine">
                Ranking
            </div>
            <div style={{width:"100%",display:"flex",justifyContent:"center",alignItems:"center",flexDirection:"column",marginTop:"10px"}}>
                <div className="textLeftAligned">
                    <em>
                        Im Ranking wird gezeigt, wie nachhaltig du dich im Vergleich zu anderen Nutzern fortbewegst. Du steigst in der Bewertung,
                        wenn du nachhaltige Routenoptionen wählst und sinkst bei umweltbelastenden Wahlen.
                        Um eine absolvierte Route werten zu lassen, muss zusätzlich ein Nachweis hinterlegt werden.

                    </em>
                </div>
                <div className="textLeftAligned" style={{marginTop: "0", marginBottom: "0"}}>
                    {position !== undefined ? `Du bist aktuell auf dem ${position}. Platz`: "Hinterlege für deine Reisen Nachweise, um auch beim Ranking mitzumachen!"}
                </div>
                <div className="text">
                    {position !== 1 ? "Streng dich weiter an, um an die Spitze des Rankings zu kommen!": ""}
                </div>
                <div className="rankingTable">
                    {renderRanking()}
                </div>
            </div>
        </div>

    </>
}