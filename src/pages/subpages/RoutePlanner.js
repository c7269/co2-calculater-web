import React, { useState } from "react";

import DirectionsCarIcon from "@mui/icons-material/DirectionsCar";
import DirectionsSubwayIcon from "@mui/icons-material/DirectionsSubway";
import DirectionsBusIcon from "@mui/icons-material/DirectionsBus";
import DirectionsBikeIcon from "@mui/icons-material/DirectionsBike";
import Co2Icon from "@mui/icons-material/Co2";
import "../../styles/standard.css";
import "../../styles/routePlanner.css";
import { Button } from "@mui/material";
import PositionAutocomplete from "../../components/PositionAutocomplete";
import axios from "axios";
import DialogContainer from "../../components/Dialog";

export const RoutePlanner = (props) => {
  const [clicked, setClicked] = useState(true);
  const [startPoint, setStartPoint] = useState(undefined);
  const [endPoint, setEndPoint] = useState("");
  const [kilometers, setKilometers] = useState(0);
  const [loading, isLoading] = useState(true);
  const [rows, setRows] = useState([
    {
      Icon: DirectionsCarIcon,
      Factor: 150,
      Pollution: 0.002,
    },
    {
      Icon: DirectionsSubwayIcon,
      Factor: 40,
      Pollution: 0,
    },
    {
      Icon: DirectionsBusIcon,
      Factor: 20,
      Pollution: 2,
    },
    {
      Icon: DirectionsBikeIcon,
      Factor: 0,
      Pollution: 0,
    },
  ]);
  const renderTable = () => {
    const list = [];
    rows.forEach((Row) => {
      list.push(
        <tr style={{ height: "25%" }}>
          <td
            style={{
              width: "25%",
              justifyContent: "center",
              justifyItems: "center",
            }}
          >
            <Row.Icon className="routeOptionsTransportationRow"></Row.Icon>
          </td>
          <td
            style={{
              justifyContent: "center",
              justifyItems: "center",
              width: "25%",
            }}
          >
            <Co2Icon className="routeOptionsPollutionRow"></Co2Icon>
          </td>
          <td className="routeText">
            <b className="routeTextBold">{Row.Pollution} </b>kg
          </td>
        </tr>
      );
    });
    return list;
  };

  const handleClick = async () => {
    if (startPoint === "" || endPoint === "") setClicked(true);
    else {
      var pos1 = await axios.get(
        `https://geocoder.ls.hereapi.com/6.2/geocode.json?apiKey=EEAnGX6cMH5_ScGwLQqU40IYYGacZDojipUmbpVCHdg&jsonattributes=1&gen=9&locationid=${startPoint.locationId}`
      );
      var pos2 = await axios.get(
        `https://geocoder.ls.hereapi.com/6.2/geocode.json?apiKey=EEAnGX6cMH5_ScGwLQqU40IYYGacZDojipUmbpVCHdg&jsonattributes=1&gen=9&locationid=${endPoint.locationId}`
      );
      var pos1Data = {
        latitude:
          pos1.data.response.view[0].result[0].location.displayPosition
            .latitude,
        longitude:
          pos1.data.response.view[0].result[0].location.displayPosition
            .longitude,
      };
      var pos2Data = {
        latitude:
          pos2.data.response.view[0].result[0].location.displayPosition
            .latitude,
        longitude:
          pos2.data.response.view[0].result[0].location.displayPosition
            .longitude,
      };

      var km = await axios.get(
        "https://router.project-osrm.org/route/v1/driving/" +
          pos1Data.longitude +
          "," +
          pos1Data.latitude +
          ";" +
          pos2Data.longitude +
          "," +
          pos2Data.latitude
      );
      km = km.data.routes[0];
      var distance = km.distance / 1000; // in kilometers
      var durationCar = km.duration / 60;

      var rowCopy = rows;
      rowCopy[0].Pollution =
        Math.round(
          ((rowCopy[0].Factor * distance) / 1000 + Number.EPSILON) * 100
        ) / 100;
      rowCopy[0].Time = Math.round(durationCar + Number.EPSILON);
      rowCopy[1].Pollution =
        Math.round(
          ((rowCopy[1].Factor * distance) / 1000 + Number.EPSILON) * 100
        ) / 100;
      rowCopy[2].Pollution =
        Math.round(
          ((rowCopy[2].Factor * distance) / 1000 + Number.EPSILON) * 100
        ) / 100;
      rowCopy[3].Time = Math.round((distance / 17) * 60 + Number.EPSILON);

      setClicked(false);
      setKilometers(Math.round((durationCar + Number.EPSILON) * 10) / 10);
      setRows(rowCopy);
      isLoading(false);
    }
  };

  return clicked ? (
    <div style={{ height: "100%", width: "100%" }}>
      <div className="headLine">Route planen</div>
        <div className="textLeftAligned">
          <em>
            Gib Beginn und Ende deiner zu planenden Reise an. <br />
            Im Anschluss werden dir verschiedenen Reisemittel dargestellt.
          </em>
        </div>
      <table
        style={{
          borderCollapse: "separate",
          borderSpacing: "1em",
          width: "95%",
          marginLeft: "5%",
        }}
      >
        <tr>
          <td className="routeFirstColumn">Start</td>
          <td className="autoCompleteDiv">
            <PositionAutocomplete
              setSelectedOption={setStartPoint}
              height={"60px"}
              additionalStyles={{ height: "60px" }}
            />
          </td>
          <td className="autoCompleteFiller" />
        </tr>
        <tr>
          <td className="routeFirstColumn">Ziel</td>
          <td className="autoCompleteDiv">
            <PositionAutocomplete
              setSelectedOption={setEndPoint}
              height={"60px"}
              additionalStyles={{ height: "60px" }}
            />
          </td>
          <td className="autoCompleteFiller" />
        </tr>
        <tr>
          <td className="routeFirstColumn"/>
          <td className="autoCompleteDiv">
            <DialogContainer
                setStartPoint={setStartPoint}
                setEndPoint={setEndPoint}
                handleClick={handleClick}
                setKilometers={setKilometers}
                setRows={setRows}
                rows={rows}
                isLoading={isLoading}
                setClicked={setClicked}
            />
          </td>
          <td className="autoCompleteFiller" />
        </tr>
        <tr>
          <td className="routeFirstColumn"/>
          <td>
            <Button
              id="buttonLogin"
              style={{
                width: "100%",
                height: "60px",
                cursor: "pointer",
                marginBottom: "5%",
              }}
              onClick={() => {
                handleClick();
              }}
            >
              Berechnen
            </Button>
          </td>
        </tr>
      </table>
    </div>
  ) : (
    <div style={{ height: "100%", width: "100%" }}>
      {loading ? (
        ""
      ) : (
        <>
          <div className="headLine">Route planen</div>
          <div className="textLeftAligned">
            <em>Deine gewählte Reise von </em> <br />
            {startPoint.label}
            <br /> <em>nach</em> {endPoint.label} <em>beträgt</em>
            <br /> {kilometers} km.
          </div>
          <div className="routeSubHeadline">
            In der folgenden Liste ist der Vergleich zwischen Auto, Zug, Bus
            oder Fahrrad.
          </div>
          <div className="routeOptions">
            <table style={{ height: "100%", width: "90%", margin: "5%" }}>
              {renderTable()}
            </table>
          </div>
        </>
      )}
    </div>
  );
};
