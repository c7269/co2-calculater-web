import { Button } from "@mui/material";
import React, { useState } from "react";
import "../../styles/standard.css";
import "../../styles/signup.css";
import axios from "axios";

export const Setting = (props) => {
  const [id, setID] = useState(props.user.id);
  const [email, setEmail] = useState(props.user.email);
  const [password, setPassword] = useState(props.user.password);
  return (
    <>
      <div className="headLine">
        Einstellung
      </div>
      <div className="divContainerSignUp">
        <div
          style={{
            height: "100%",
            width: "30%",
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            flexDirection: "column",
            textAlign: "center",
            color: "black",
          }}
        >
          <label className="labelSignUp">ID</label>
          <label className="labelSignUp">Email</label>
          <label className="labelSignUp">Password</label>
        </div>
        <div
          style={{
            height: "100%",
            width: "70%",
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            flexDirection: "column",
          }}
        >
          <input
            type="text"
            className="inputSignUp"
            value={id}
            readOnly
            onChange={(e) => {
                setID(e.target.value);
            }}
          />
          <input
            type="text"
            className="inputSignUp"
            value={email}
            onChange={(e) => {
                setEmail(e.target.value);
            }}
          />
          <input
            type="password"
            className="inputSignUp"
            value={password}
            onChange={(e) => {
                setPassword(e.target.value);
            }}
          />
        </div>
      </div>
      <div
        style={{
          width: "90%",
          display: "flex",
          flexDirection: "column",
          marginLeft: "5%",
          marginRight: "5%",
        }}
      >
        <div style={{ width: "100%", textAlign: "center", marginTop: "5%", display: "flex" }}>
          <Button
            style={{ width: "60%", marginLeft: "35%" }}
            id="buttonLogin"
            onClick={() => {
              axios
                .post("/change", {
                  id: id,
                  email: email,
                  password: password,
                })
                .then((res) => {
                  if (res == true) {
                    props.setUser({ ...props.user, id, email, password });
                  }
                })
                .catch((error) => alert("Failed to change personal data"));
            }}
          >
            Persönliche Daten ändern
          </Button>
        </div>
      </div>
    </>
  );
};
