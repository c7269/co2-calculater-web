import { Button } from "@mui/material"
import "../../styles/standard.css"
import "../../styles/signup.css"
import {useState} from "react";
import axios from "axios";

export const Signup = (props) => {
    const [email, setEmail] = useState(undefined)
    const [password, setPassword] = useState(undefined)
    const [id, setID] = useState(undefined)

    const handleSignUp = () => {
        axios.post('/signup', {id, email, password})
            .then(res => {alert('Sign up successful. Proceed to login'); props.setCurrentPage("Login")})
            .catch(error => {alert('Sign up failed due to "'+error.response.data.error+'"')})
    }

    return <>
        <div style={{ height: "100%", width: "100%" }}>
            <div className="headLine">
                Registrieren
            </div>
            <div style={{width:"100%",display:"flex",flexDirection:"column"}}>
                <div className="signUpInformation">
                    Unser Service ist komplett kostenlos und die Anmeldung erfolgt ohne Angabe personenbezogener Informationen. <br /><br />
                    Wähle eine ID und ein aussagekräftiges Passwort, mit dem du dich folglich anmelden kannst. <br />
                    Um am optionalen Ranking-Spiel teilzunehmen, wird deine E-Mail Adresse benötigt.
                </div>
            </div>
            <div className="divContainerSignUp">
                <div style={{height:"100%",width:"30%",display:"flex",justifyContent:"center",alignItems:"center",flexDirection:"column",textAlign:"center",color:"black"}}>
                    <label className="labelSignUp">ID</label>
                    <label className="labelSignUp">E-Mail Adresse <div className="optional">(optional)</div></label>
                    <label className="labelSignUp">Passwort</label>

                </div>
                <div style={{height:"100%",width:"70%",display:"flex",justifyContent:"center",alignItems:"center",flexDirection:"column"}}>
                    <input type="text" className="inputSignUp" input={id} onInput={e => setID((e.target.value))}></input>
                    <input type="email" className="inputSignUp" input={email} onInput={e => setEmail((e.target.value))}></input>
                    <input type="password" className="inputSignUp" input={password} onInput={e => setPassword((e.target.value))}></input>
                </div>
            </div>
            <div style={{width:"90%",display:"flex",flexDirection:"column", marginLeft: "5%", marginRight: "5%"}}>
                <div style={{width: "70%", marginLeft: "35%"}}>
                    <Button style={{width: "85%"}} variant="contained" id="buttonLogin" onClick={() => {handleSignUp()}}>Registrieren</Button>
                    <div className="textLeftAligned" style={{margin: 0, display: "flex",textAlign: "center", justifyContent:"center",alignItems:"center", width: "85%", marginTop: "2.5%", flexDirection: "column"}}>
                        Du hast schon einen Account? <br></br>
                        Dann kannst du dich jetzt einfach anmelden
                        <Button variant="text" style={{alignItems: "center", display: "flex", color: "black", justifyContent: "center"}}
                                onClick={() => {props.setCurrentPage("Login")}}
                        >Registrieren</Button>
                    </div>
                </div>
            </div>
        </div>

    </>
}